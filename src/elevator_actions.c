/* edit this file to implement action functions. 
  You can also regenerate this file to fill with auto generated stubs,
 but any existing content will be OVERWRITTEN! */
#include "elevator_actions.h"
#define ON 0
#define OFF 1
void goto_floor1(){
	GPIOSetBitValue(0,9, OFF);
	GPIOSetBitValue(0,1, ON);
	GPIOSetBitValue(0,2, ON);
	GPIOSetBitValue(0,3, OFF);
	GPIOSetBitValue(0,4, OFF);
	GPIOSetBitValue(0,5, OFF);
	GPIOSetBitValue(0,7, OFF);
}

void goto_floor2(){
	GPIOSetBitValue(0,9, ON);
	GPIOSetBitValue(0,1, ON);
	GPIOSetBitValue(0,2, OFF);
	GPIOSetBitValue(0,3, ON);
	GPIOSetBitValue(0,4, ON);
	GPIOSetBitValue(0,5, OFF);
	GPIOSetBitValue(0,7, ON);
}

void goto_floor3(){
	GPIOSetBitValue(0,9, ON);
	GPIOSetBitValue(0,1, ON);
	GPIOSetBitValue(0,2, ON);
	GPIOSetBitValue(0,3, ON);
	GPIOSetBitValue(0,4, OFF);
	GPIOSetBitValue(0,5, OFF);
	GPIOSetBitValue(0,7, ON);
}

void output_1(){

}

void delay(){
	for(int i=0; i<500000;i+=2)
	{
	//	printf("delay~~~");
	}
}
