/* ****************************************************** */
/* State machine generated by Red State Machine generator */
/* Mon Nov 06 22:19:23 EET 2017                           */
/* This file was automatically generated and will be      */
/* OVERWRITTEN if regenerated.                            */
/*  o Do not make changes to this file directly.          */
/*  o Do define your actions functions in                 */
/*     elevator_actions.c                                 */
/* ****************************************************** */
void delay(); /* Action: delay */
void goto_floor1(); /* Action: goto_floor1 */
void goto_floor2(); /* Action: goto_floor2 */
void goto_floor3(); /* Action: goto_floor3 */
void output_1(); /* Action: output 1 */
